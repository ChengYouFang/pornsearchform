﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

namespace CYFang
{
    public partial class ProcessBarForm : Form
    {

        protected internal class PornWorker
        {
            /// <summary>
            /// PornInfo
            /// </summary>
            struct PornInfo
            {
                /// <summary>
                /// video id
                /// </summary>
                public string number;
                /// <summary>
                /// title
                /// </summary>
                public string title;
                /// <summary>
                /// video url
                /// </summary>
                public string url;
                /// <summary>
                /// thumbnail
                /// </summary>
                public string image;
            };

            /// <summary>
            /// Type
            /// </summary>
            public enum WorkerType
            {
                Master = 1,
                Slave = 2,
                Reader = 3
            };

            /// <summary>
            /// worker
            /// </summary>
            private BackgroundWorker worker = new BackgroundWorker();

            /// <summary>
            /// 
            /// </summary>
            private DataGridView dgv;

            /// <summary>
            /// 
            /// </summary>
            private string url;

            /// <summary>
            /// 
            /// </summary>
            private string title;

            /// <summary>
            /// Worker Type
            /// </summary>
            private WorkerType type;

            /// <summary>
            /// 
            /// </summary>
            private List<PornInfo> pornList;

            /// <summary>
            /// 
            /// </summary>
            private String filter;

            /// <summary>
            /// 
            /// </summary>
            protected internal bool isStop = false;

            /// <summary>
            /// 建構子
            /// </summary>
            public PornWorker() : this(WorkerType.Master)
            {
                CheckImageFolder();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="type"></param>
            public PornWorker(WorkerType type, DataGridView dgv = null, String filter = null)
            {
                this.type = type;
                this.dgv = dgv;
                this.filter = filter;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="url"></param>
            /// <param name="title"></param>
            /// <param name="type"></param>
            private PornWorker(String url, String title, WorkerType type = WorkerType.Slave)
            {
                this.url = url;
                this.title = title;
                this.type = type;
            }

            /// <summary>
            /// 
            /// </summary>
            public void Run()
            {
                worker.DoWork += Worker_DoWork;
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;
                worker.RunWorkerAsync();
            }

            /// <summary>
            /// 
            /// </summary>
            public void Stop()
            {
                worker.CancelAsync();
            }

            /// <summary>
            /// 檢查是否有新影片
            /// </summary>
            /// <returns></returns>
            private bool CheckUpdate()
            {
                return false;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public static bool CheckConfig()
            {
                return File.Exists(PornSearchForm.CONFIG_PATH);
            }

            /// <summary>
            /// Check if the folder exists
            /// </summary>
            private void CheckImageFolder()
            {
                if (Directory.Exists(PornSearchForm.IMAGE_PATH) == false)
                {
                    Directory.CreateDirectory(PornSearchForm.IMAGE_PATH);
                }
            }

            /// <summary>
            /// Get Porn list
            /// </summary>
            private PornInfo[] GetPornList()
            {
                var list = new List<PornInfo>();
                using (var client = new WebClient())
                {
                    client.Encoding = Encoding.UTF8;
                    var content = client.DownloadString("http://18av.mm-cg.com/");
                    content = Regex.Match(content, "<br><a href=\".* ").Value.Replace("<br><a href=\"", "").Replace("\">", ";").Replace("</a>", ";").Replace("<br><a ", "");
                    var infos = content.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                    for (int index = 0; index < infos.Length; index += 2)
                    {
                        list.Add(new PornInfo
                        {
                            url = infos[index],
                            title = infos[index + 1]
                        });
                    }

                }
                return list.ToArray();
            }

            /// <summary>
            /// Get
            /// </summary>
            /// <returns></returns>
            private void SetPornInfos()
            {
                using (var client = new WebClient())
                {
                    //UTF8
                    client.Encoding = Encoding.UTF8;

                    //取得網頁內容
                    client.DownloadStringAsync(new Uri(url));
                    client.DownloadStringCompleted += (s, e) =>
                    {
                        #region 取得第一個影片ID
                        var pTitle = title.Replace("收錄影片", "");
                        var pNumber = pTitle.Substring(0, pTitle.IndexOf('~'));
                        pNumber = "http://18av.mm-cg.com/18av/" + pNumber + ".html";
                        #endregion 取得第一個影片ID

                        #region 取得所有影片資料
                        var content = e.Result;
                        var start = content.IndexOf(pNumber);
                        var end = content.LastIndexOf(pNumber);
                        if (end - start > 0)
                            content = content.Substring(start, end - start);
                        else
                            Console.WriteLine(content);
                        content = content.Substring(0, content.LastIndexOf("<br><br>"));
                        content = Regex.Replace(content, "\">\\S{1,}\\s\\w{1,}=\\\"\\w{1,}\\\"\\sborder=\\\"0\\\"\\ssrc=\\\"", " ")
                            .Replace("\" '+' width=\"250\" height=\"170\" alt=\"", " ");
                        content = Regex.Replace(content, "\\\"\\sjizcg=.*?.</img></a>\'[+]\'", ";;;;");
                        content = Regex.Replace(content, "<a class=\"\\w{1,}\" href=\"", " ");

                        #endregion 取得所有影片資料

                        //切割字串
                        var infos = content.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                        pornList = new List<PornInfo>();
                        #region Init


                        #endregion Init
                        StringBuilder sb = new StringBuilder();

                        //該頁面所有影片
                        foreach (var info in infos)
                        {
                            //圖片儲存路徑
                            string imagePath = null;

                            if (info.Contains(";;;;"))
                            {
                                sb.Append(info.Replace(";;;;", ""));
                                var array = sb.ToString().Split(' ');

                                int min = array[0].LastIndexOf('/') + 1;
                                int max = array[0].LastIndexOf('.');
                                var imageNumber = array[0].Substring(min, max - min);

                                imagePath = PornSearchForm.IMAGE_PATH + imageNumber;
                                if (File.Exists(imagePath) == false)
                                {
                                    client.DownloadFile(array[1], imagePath + ".jpg");
                                }

                                string title = string.Empty;
                                for (int p = 2; p < array.Length; p++)
                                    title += array[p];


                                pornList.Add(new PornInfo
                                {
                                    url = array[0],
                                    image = imagePath + ".jpg",
                                    title = title,
                                    number = imageNumber
                                });

                                sb = new StringBuilder();
                            }
                            else
                            {
                                sb.Append(info + " ");
                            }
                        }
                        isStop = true;
                    };
                }
            }

            /// <summary>
            /// 
            /// </summary>
            private void ReaderPornInfos()
            {
                XDocument doc = XDocument.Load(PornSearchForm.CONFIG_PATH);
                var list = doc.Element("list");
                pornList = new List<PornInfo>();
                foreach (var m in list.Elements())
                {
                    pornList.Add(new PornInfo
                    {
                        number = m.Element("id").Value,
                        url = m.Element("url").Value,
                        title = m.Element("title").Value,
                        image = m.Element("image").Value
                    });
                }

                worker.ProgressChanged += (s, e) =>
                {
                    if (dgv.Rows.Count > 0)
                        dgv.Rows.Clear();

                    if (string.IsNullOrEmpty(filter))
                    {
                        foreach (var p in pornList)
                        {
                            dgv.Rows.Add(p.number, p.url, p.title, p.image);
                        }
                    }
                    else
                    {
                        foreach (var p in pornList.Where(pp => pp.title.Contains(filter) == true))
                        {
                            dgv.Rows.Add(p.number, p.url, p.title, p.image);
                        }
                    }
                };
                worker.ReportProgress(0);
            }

            /// <summary>
            /// 
            /// </summary>
            private void SavePornInfo(PornWorker[] slaveWorker)
            {
                XDocument doc = new XDocument();
                var config = new XElement("list");

                foreach (var s in slaveWorker)
                {
                    if (s.pornList != null)
                    {
                        foreach (var l in s.pornList)
                        {

                            var porn = new XElement("video",
                                new XElement("id", l.number),
                                new XElement("url", l.url),
                                new XElement("title", l.title),
                                new XElement("image", l.image));
                            config.Add(porn);
                        }
                    }
                }
                doc.Add(config);
                doc.Save(PornSearchForm.CONFIG_PATH);
                MessageBox.Show("更新完畢，我們將為你重啟程式！", "更新完畢");
                Application.Restart();
            }


            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void Worker_DoWork(object sender, DoWorkEventArgs e)
            {
                if (type == WorkerType.Master)
                {
                    var list = GetPornList().ToArray();
                    int maxLength = list.Length;

                    var slaveWorker = new PornWorker[maxLength];
                    for (int index = 0; index < maxLength; index++)
                    {
                        try
                        {
                            var min = list[index].url.LastIndexOf('/') + 1;
                            var max = list[index].url.LastIndexOf('.');
                            var number = list[index].url.Substring(min, max - min);
                            if (Convert.ToInt32(number) > 1000)
                            {
                                slaveWorker[index] = new PornWorker(list[index].url, list[index].title);
                                slaveWorker[index].Run();
                            }
                            else
                            {
                                slaveWorker[index] = new PornWorker();
                                slaveWorker[index].isStop = true;
                            }


                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine(ex.StackTrace);
                            continue;
                        }

                    }

                    while (isStop == false && worker.CancellationPending == false)
                    {
                        if (worker.CancellationPending)
                        {
                            foreach (var s in slaveWorker)
                            {
                                s.Stop();
                            }

                            SavePornInfo(slaveWorker);
                            e.Cancel = true;
                            return;
                        }
                        else
                        {
                            var isSuccess = slaveWorker.Where(s => s.isStop == true).Select(s => s).Count() == slaveWorker.Count();
                            isStop = isSuccess;

                            if (isSuccess == true)
                            {
                                SavePornInfo(slaveWorker);
                                e.Cancel = true;
                                return;
                            }
                        }
                    }
                }
                else if (type == WorkerType.Slave)
                {
                    SetPornInfos();
                }
                else if (type == WorkerType.Reader)
                {
                    ReaderPornInfos();
                }
            }
        }

        /// <summary>
        /// PornWorker
        /// </summary>
        private PornWorker worker;

        /// <summary>
        /// 
        /// </summary>
        private int count = 0;

        /// <summary>
        /// 
        /// </summary>
        public ProcessBarForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;

            if (PornWorker.CheckConfig() == false)
            {
                timer1.Tick += (s, e) =>
                {
                    count = ++count > 100 ? 0 : count;
                    progressBar1.Value = count;
                };
                timer1.Interval = 100;
                timer1.Start();
                worker = new PornWorker();
                worker.Run();
            }
            else
            {
                this.Hide();
                PornSearchForm searchForm = new PornSearchForm();
                searchForm.ShowDialog();
            }
        }

    }
}
