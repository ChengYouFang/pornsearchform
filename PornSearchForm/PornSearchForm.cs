﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using static CYFang.ProcessBarForm;
using static CYFang.ProcessBarForm.PornWorker;

namespace CYFang
{
    public partial class PornSearchForm : Form
    {
        /// <summary>
        /// Config path
        /// </summary>
        public static readonly String CONFIG_PATH = Directory.GetCurrentDirectory() + @"\video.source";

        /// <summary>
        /// Image path
        /// </summary>
        public static readonly String IMAGE_PATH = Directory.GetCurrentDirectory() + @"\img\";

        /// <summary>
        /// PornWorker
        /// </summary>
        private PornWorker worker;

        /// <summary>
        /// 
        /// </summary>
        public PornSearchForm()
        {
            InitializeComponent();

            dataGridView1.Columns.Add("id", "編號");
            dataGridView1.Columns.Add("url", "網址");
            dataGridView1.Columns.Add("title", "標題");
            dataGridView1.Columns[2].Width = 480;
            dataGridView1.Columns.Add("image","縮圖");

            dataGridView1.RowHeadersVisible = false;
            dataGridView1.Columns[1].Visible = false;
            dataGridView1.AllowUserToOrderColumns = false;
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            /*
            DataGridViewImageColumn imageColumn = new DataGridViewImageColumn();
            imageColumn.HeaderText = "image";
            imageColumn.HeaderText = "縮圖";
            imageColumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
            imageColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            imageColumn.MinimumWidth = 100;
            imageColumn.Width = 160;
            dataGridView1.RowTemplate.Height = 100;
            dataGridView1.Columns.Add(imageColumn);
            */

            dataGridView1.CellClick += (s, e) =>
            {
                if (dataGridView1.Columns[e.ColumnIndex].Name == "title")
                {
                    if (e.RowIndex > -1)
                    {
                        Process p = Process.Start("IEXPLORE.EXE", dataGridView1.Rows[e.RowIndex].Cells["url"].Value.ToString());
                    }
                }
                else if (dataGridView1.Columns[e.ColumnIndex].Name == "image")
                {
                    if (e.RowIndex > -1)
                    {
                        Process p = Process.Start(dataGridView1.Rows[e.RowIndex].Cells["image"].Value.ToString());
                    }
                }
            };

            worker = new PornWorker(WorkerType.Reader, dataGridView1);
            worker.Run();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PornForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            worker.Stop();
            Application.Exit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var str = textBox1.Text;
            if (string.IsNullOrEmpty(str) == false)
            {
                worker = new PornWorker(WorkerType.Reader, dataGridView1, str);
                worker.Run();
            }
            else
            {
                worker = new PornWorker(WorkerType.Reader, dataGridView1);
                worker.Run();
            }
        }

    }
}
